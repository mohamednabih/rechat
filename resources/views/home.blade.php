@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        <a href="{{url('public-chat')}}">الذهاب الى شات عام</a>
        <br>
        @if(count(Auth::user()->Rooms) > 0)
        @foreach(Auth::user()->Rooms as $room )
            <a href="{{url('chat/' . $room->id)}}">الذهاب الى غرفة رقم {{$room->id}}</a>
            <br>
        @endforeach
        @endif

        </div>
    </div>
</div>
@endsection
