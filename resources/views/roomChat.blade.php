@extends('layouts.app')

@push('style')
<link href="{{ asset('css/oneChat.css') }}" rel="stylesheet">
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(isset($messages) && isset($room))
            <div class="card">
            <chat-component :messages="{{$messages}}" :room="{{$room}}"></chat-component>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection

