<?php

use App\User;

/********* start images folder work *********/
if(!function_exists('uploadImage')){
    function uploadImage($photo, $dir)
    {
        $dir1 = 'images/'. $dir;
        if (!file_exists($dir1)) {
            mkdir($dir1, 0777, true);
        }
        #upload image
        $name = date('d-m-y') . time() . rand(1 ,1000) . '.' . $photo->getClientOriginalExtension();
        $photo->move($dir1 . '/', $name);
        return '/' . $dir1 . '/' . $name;
    }
}
if(!function_exists('getImage')){
    function getImage($photo)
    {
        return url('' . $photo);
    }
}
if(!function_exists('deleteOldImage')){
    function deleteOldImage($photoWithDirection)
    {
        if($photoWithDirection != null && $photoWithDirection != '/images/default.png') {
            // need to delete word '/public' from name - public_path
            //$pathToFile = public_path(str_replace('/public', "", $photoWithDirection));
            $pathToFile = public_path($photoWithDirection);
            if(file_exists($pathToFile)){
                unlink($pathToFile);
            }
        }
    }
}
if(!function_exists('defaultImage')){
    function defaultImage(){
        return url('images/soon.gif');
    }
}
/********* end images folder work *********/
// get user status [(activation = 1) => active, (activation = 0) => non-active, (checked = 0) => blocked]
if(!function_exists('userStatus')){
    function userStatus($id){
        $user = User::whereId($id)->first();
        if($user->activation == 0){
            return 'non-active';
        }elseif($user->checked == 0){
            return 'blocked';
        }else{
            return 'active';
        }
    }
};
if(!function_exists('ApiResponse')){
    function ApiResponse($key,$msg,$data = [],$anotherKey = []){
        $allResponse['key']     = (string) $key;
        $allResponse['msg']     = (string)  $msg;
        $allResponse['data']    = $data ;

        if(!empty($anotherKey)){
            foreach ($anotherKey as $key => $value) {
                $allResponse[$key] = (string) $value;
            }
        }
        return response()->json($allResponse);
    }
}
