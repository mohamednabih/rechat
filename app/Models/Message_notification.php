<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message_notification extends Model
{
    protected $fillable = [
        'message_id', 'room_id', 'is_seen','is_sender','flagged','is_delete'
    ];

    public function Message(){
        return $this->belongsTo('App\Models\Message');
    }
}
