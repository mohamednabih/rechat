<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'type', 'order_id', 'user_id','private','other_user_id'
    ];

    // direct throw pivot table return users
    public function Users()
    {
        return $this->belongsToMany('App\User', 'room_users',
            'room_id', 'user_id');
    }

    // who create the room
    public function Owner(){
        return $this->belongsTo('App\User','user_id','id');
    }

    // direct messages from messages table
    public function DirectMessages()
    {
        return $this->hasMany('App\Models\Message');
    }

    // get from message_notification table - where copy message for every room user
    public function Messages()
    {
        return $this->hasMany('App\Models\Message_notification')->with('Message');
    }

    public function LastMessage(){
        return $this->hasOne('App\Models\Message','id','last_message_id');
    }

    // public function Order(){
    //     return $this->belongsTo('App\Models\Order');
    // }
}
