<?php

namespace App\Http\Controllers\api;

use App\User;
use App\Models\Room;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RoomsResource;
use App\Http\Resources\MessageResource;
use Illuminate\Support\Facades\Validator;

class ChatController extends Controller
{

    /******************************** start chat work ***********************/
    public function UserRooms(Request $request){
        $validate = Validator::make($request->all(), [
            'user_id'           => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return ApiResponse(0, $validate->errors()->first());
        }

        $user = User::whereId($request->user_id)->first();

        $rooms = $user->Rooms;

        /** send data **/
        $data   = $rooms;
        $data   = RoomsResource::collection($rooms);
        $msg    = trans('api.send');
        return ApiResponse(1,$msg,$data,['user_status'=> userStatus($user->id)]);
    }
    public function RoomMessages(Request $request){
        $validate = Validator::make($request->all(), [
            'user_id'           => 'required|exists:users,id',
            'room_id'           => 'required|exists:rooms,id',
        ]);

        /** Send Error Massages **/
		if ($validate->fails()) {
            return ApiResponse(0, $validate->errors()->first());
        }

        $user = User::whereId($request->user_id)->first();
        $messages = getRoomMessages($request->room_id,$user->id);

        /** send data **/
        $data = MessageResource::collection($messages);
        $msg = trans('api.send');
        return ApiResponse(1,$msg,$data,['user_status'=> userStatus($user->id)]);
    }
    public function SaveMessage(Request $request){
        $validate = Validator::make($request->all(), [
            'user_id'   => 'required|exists:users,id',
            'room_id'   => 'required|exists:rooms,id',
            'message'   => 'required',
        ]);

        /** Send Error Massages **/
		if ($validate->fails()) {
            return ApiResponse(0, $validate->errors()->first());
        }

        $user = User::whereId($request->user_id)->first();
        $room = Room::whereId($request->room_id)->first();

        if(!in_array($request->user_id,$room->Users->pluck('id')->toArray())){
            return ApiResponse(0, 'not amember');
        }

        if(gettype($request->message) == 'object'){
            // validate mimes
            $path = $_FILES['message']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            if(!in_array($ext,['jpeg','jpg','gif','png','pdf'])){
                return ApiResponse(0, 'file ext should be [jpeg,jpg,gif,png,pdf]');
            }

            $filename       = uploadImage($request->message, 'rooms/'. $request->room_id);
            $lastMessage    = saveMessage($request->room_id,$filename,$request->user_id,'file');
        }elseif(gettype($request->message) == 'string'){
            $lastMessage    = saveMessage($request->room_id,$request->message,$request->user_id);
        }

        /** send data **/
        $data = new MessageResource($lastMessage);
        $msg = trans('api.send');
        return ApiResponse(1,$msg,$data,['user_status'=> userStatus($user->id)]);
    }
    public function DeleteMessage(Request $request){
        $validate = Validator::make($request->all(), [
            'user_id'       => 'required|exists:users,id',
            'message_id'    => 'required|exists:message_notifications,id',
        ]);

        /** Send Error Massages **/
		if ($validate->fails()) {
            return ApiResponse(0, $validate->errors()->first());
        }

        $user = User::whereId($request->user_id)->first();

        deleteMessage($request->message_id,$request->user_id);

        /** send data **/
        $msg = trans('api.deleted');
        return ApiResponse(1,$msg,[],['user_status'=> userStatus($user->id)]);
    }

    public function UnreadMessages(Request $request){
        $validate = Validator::make($request->all(), [
            'user_id'           => 'required|exists:users,id',
            'room_id'           => 'required|exists:rooms,id',
        ]);

        /** Send Error Massages **/
		if ($validate->fails()) {
            return ApiResponse(0, $validate->errors()->first());
        }

        $user       = User::whereId($request->user_id)->first();
        $messages   = unreadRoomMessages($request->room_id,$request->user_id);

        /** send data **/
        $data = $messages->count();
        $msg = trans('api.send');
        return ApiResponse(1,$msg,$data,['user_status'=> userStatus($user->id)]);
    }
    public function ReadAllMessages(Request $request){
        $validate = Validator::make($request->all(), [
            'user_id'           => 'required|exists:users,id',
            'room_id'           => 'required|exists:rooms,id',
        ]);

        /** Send Error Massages **/
		if ($validate->fails()) {
            return ApiResponse(0, $validate->errors()->first());
        }

        $user = User::whereId($request->user_id)->first();
        readAllRoomMessages($request->room_id,$user->id);

        /** send data **/
        $data = [];
        $msg = trans('api.send');
        return ApiResponse(1,$msg,$data,['user_status'=> userStatus($user->id)]);
    }
    /******************************** end chat work ***********************/
}
