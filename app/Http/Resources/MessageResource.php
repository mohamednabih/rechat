<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{

    public function toArray($request)
    {
        $message = [
            'id'            => $this->Message->id,
            'body'          => (string) ($this->Message->type == 'file')? getImage($this->Message->body):$this->Message->body,
            'room_id'       => $this->Message->room_id,
            'user_id'       => $this->Message->user_id,
            'type'          => $this->Message->type,
        ];
        return [
            'id'            => $this->id,
            'message_body'  => (string) ($this->Message->type == 'file')? getImage($this->Message->body):$this->Message->body,
            'room_id'       => $this->room_id,
            // this type for mobile developer - return image,pdf or text
            'type'          => $this->Message->type == 'file' ? (ltrim(strrchr($this->Message->body, "."),'.') == 'pdf' ? 'pdf' : 'image'):'text',
            'user_id'       => $this->user_id,
            'is_seen'       => (string) $this->is_seen,
            'is_sender'     => (string) $this->is_sender,
            'flagged'       => (string) $this->flagged,
            'is_delete'     => (string) $this->is_delete,
            'created_at'    => (string) $this->created_at,
            // from helper - saveMessage - get other room users for emit
            'other_users'   => isset($this->other_users) ? $this->other_users:[],
            'message'       => $message,

        ];
    }
}
