<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

##### chat #####
Route::post('user-rooms', 'api\ChatController@UserRooms');

Route::post('send-message', 'api\ChatController@SaveMessage');
Route::post('room-messages', 'api\ChatController@RoomMessages');
Route::post('delete-message', 'api\ChatController@DeleteMessage');

Route::post('unread-messages', 'api\ChatController@UnreadMessages');
Route::post('read-all-messages', 'api\ChatController@ReadAllMessages');
