<?php


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

##### chat #####
Route::get('rooms', 'ChatController@UserRooms');
Route::get('chat/{room}', 'ChatController@GetChat');
Route::post('save-message', 'ChatController@SaveMessage');

Route::get('public-chat', 'ChatController@PublicChat');
Route::get('chat-list','ChatController@OtherUsers');
Route::get('create-private-room/{user}','ChatController@NewPrivateRoom');


//Route::post('delete-message', 'api\ChatController@DeleteMessage');
//Route::post('unread-messages', 'api\ChatController@UnreadMessages');
//Route::post('read-all-messages', 'api\ChatController@ReadAllMessages');
