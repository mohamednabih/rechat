<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /***
     * this table for original message sent and its info
     * not for show direct in queries
     * to get message use message_notifications table
     */

    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->integer('id')->unsigned()->autoIncrement();
            $table->text('body');
            $table->integer('room_id')->unsigned();
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('type')->default('text'); // text - file
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
