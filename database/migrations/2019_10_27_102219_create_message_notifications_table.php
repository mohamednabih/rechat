<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessageNotificationsTable extends Migration
{
    /***
     * this table take original message and copy it for each room user_id
     * every user has owen copy if he want to delete
     */
    public function up()
    {
        Schema::create('message_notifications', function (Blueprint $table) {
            $table->integer('id')->unsigned()->autoIncrement();
            $table->integer('message_id')->unsigned();
            $table->foreign('message_id')->references('id')->on('messages')->onDelete('cascade');
            $table->integer('room_id')->unsigned();
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('is_seen')->default(0);
            $table->integer('is_sender')->default(0);
            $table->integer('flagged')->default(0);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('message_notifications');
    }
}
