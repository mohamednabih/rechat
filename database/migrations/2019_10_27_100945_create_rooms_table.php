<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{

    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->integer('id')->unsigned()->autoIncrement();
            $table->integer('private')->default(0); // not used yet
            $table->string('type')->default('order'); // order/support
            $table->integer('order_id')->nullable(); // order/support
            $table->integer('user_id'); // creater
            $table->integer('other_user_id')->nullable(); // partner in private room
            $table->integer('last_message_id')->default(0);
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
